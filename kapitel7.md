- [Optimierung von Anfragen](#optimierung-von-anfragen)
- [Grundprinzipien der Optimierung](#grundprinzipien-der-optimierung)
- [Teilziele der Optimierung](#teilziele-der-optimierung)
- [Relationenalgebra](#relationenalgebra)
- [Wichtigte Anfrageoperationen](#wichtigte-anfrageoperationen)
- [Phasen der Optimierung](#phasen-der-optimierung)
- [Phasen der Anfragebearbeitung](#phasen-der-anfragebearbeitung)
- [Kostenmodelle und Kostenabschätzung](#kostenmodelle-und-kostenabsch-tzung)
- [Komponenten von Kostenmodellen](#komponenten-von-kostenmodellen)
  * [Ausgabe von Plänen](#ausgabe-von-pl-nen)
  * [Optimizer Hints](#optimizer-hints)
  * [Planparametrisierung](#planparametrisierung)
  * [Codeerzeugung](#codeerzeugung)

#### Optimierung von Anfragen
* Optimierer soll optimale Strategie zur Anfragebearbeitung bestimmen

#### Grundprinzipien der Optimierung
* Berechnungsweg zum Erreichen des Ergebnisses wird nicht vorgegeben (Sprache wie SQL charakterisiert lediglich Ergebnismenge)
* Berechnungsweg kann sogar nicht formuliert werden (aha)
* Relationenalgebra kann Berechnungswege vorgeben
* -> Relationenalgebra idealer Kandidat für interne Darstellung von Anfragen in bestimmten Optimierungsschritten (Natürlich.)
* __ALSO:__ In allen Operationen sollten so wenig wie möglich Seiten (Tupel) verarbeitet werden

#### Teilziele der Optimierung
* Selektion so früh wie möglich
 * -> kleine Zwischenergebnisse
* Basisoperationen wenn zusammenfassbar als einen Berechnungsschritt realsieren, keine Zwischenergebnisse Zwischenspeichern
* Nur Berechnungen ausführen die Beitrag zum Gesamtergebnis liefen
 * Redundante/leere Zwischenrealtionen aus Berechnungsplan entfernen
* Gleiche Teilausdrücke zusammenfassen
 * -> Wiederverwendbarkeit d. Zwischenergebnisse

>>>
#### Relationenalgebra
* formale Sprache um Abfrage züber relationeles Schema zu fomulieren
* Theoretische Grundlage für Abfragesprachen
* Erlaubt Relationen miteinander zu verknüpfen/reduzieren und komplexe Infos daraus herzuleiten
 
#### Wichtigte Anfrageoperationen
*  __Selektion__
 * Zeilen aus Tabelle auswählen
 * Einfaches Prädikat angebbar
 * Darstellung: σ
 * Beispiel: Beispiel: σ<sub>LName</sub>= ‚Kaffeebude' (r (PRODUKT))
*  __Projektion__
 * Spalten Auswählen
 * Notation analog zu Selektion
 * Symbol π
 * Attributname angeben, doppelte Dupel entfernt
 * Beispiel: π<sub>LName</sub> (r (PRODUKT))
*  __Verbund (Natural Join):__
 * Zwei Tabellen miteinander "verschmelzen"
 * Verknüpft Tabellen über gleich benannte Spalten
 * Symbol: ⋈
 * Beispiel: π<sub>Bezeichnung, Lname, Adresse</sub> (σ<sub>Preis<15</sub> (r (PRODUKT)) ⋈r (LIEFERANT))
>>>

#### Phasen der Optimierung
* Logische Optimierung
 * Basierung auf einer übersetzten Anfrage als Relationenalgebra Term
 * Anfrageterm wird umgeformt
 * Ohne Zugriff auf interne Schema/statistiken
 * Weder Größe d. Relation noch Ausprägung d. Indexstrukturen berücksichtigt
 * Typische Umformung: Hineinziehen von Selektionen in andere Operationen -> kleine Zwischenmenge
* Physische Optimierung
 * Aufgabe: Ergebnis der logischen Optimierung in ausführbaren Plan umformen
 * In Zugriffsplänen abstrakte Algebraoperatoren durch konkrete Algorithmen ersetzt
 * Konkrete Speicherungstechniken (Indexe, Cluster) werden berücksichtigt
 * Transformation häufig nicht eindeutig
  * Entscheidung für eine konkrete Alternative auf Basis von Kostenabschätung -- siehe Kostenbasierte Auswahl
* Kostenbasierte Auswahl
 * Statistikinformationen werden genutzt für die Auswahl eines konkreten internen Planes
 * Statistikinformationen: Größe von Tabellen, Attributen,...
 * Siehe weiterhin [Kostenmodelle und Kostenabschätzung](#kostenmodelle-und-kostenabsch-tzung)
 

#### Phasen der Anfragebearbeitung
* Anfrageübersetzung und -vereinfachung
* Logische Optimierung s.o
* Physische Optimierung s.o.
* Kostenbasierte Auswahl s.o.
* Planparametrisierung
* Codeerzeugung

#### Kostenmodelle und Kostenabschätzung
* Kostenmodel eines Anfrageprozessors wichtigste Basis für die Auswahl eines Plans
* Entscheidungskriterium für die "Optimalität"

#### Komponenten von Kostenmodellen
* Kostenfunktion:
 * Zur Abschätzung der Kosten für die Ausführung von Operationen bzw. Anfragen
* Statistiken:
 * Über die Größe der Relationen (Kradinalität, Tupelgröße)
 * Über Wertebrecieh und - verteilungen der Attribute
 * Gehen in die Kostenfunktion ein
* Formeln:
 * Zur Berechnung der Größen von (Zwischen-) Ergebnissen auf Basis der Statistiken
 
*__Zeug aus der Summry das nicht im klausurrelevanten Doc zu finden ist:__*

##### Ausgabe von Plänen
* Kommerzielle DBMS erlauben meist Optimierung in begr. Maße zu beinflueesn

##### Optimizer Hints
* Analyse der Ausführungspläne: Optimierer hat falsche Entscheidung getroffen
* -> Einzelheiten des Plans für konkrete Anfragen können direkt vorgegeben werden
 * sog. Hints

##### Planparametrisierung
* Bei vorkompilierten prepared statements können Optimierungsschritte ausgelassen werden, da bereits Ausführungsplan vorhanden
* Dann: nur noch Platzhalter in Anfrage parametrisieren mit den übergebenen Werte (Aha?)

##### Codeerzeugung
* Zugriffsplan wandelt in ausführbaren Code um
* Alternativ: Zugriffspläne direkt vom einem Interpreter verarbeitet (Ja klar...)
