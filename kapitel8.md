- [Verteilte DBMS](#verteilte-dbms)
- [Terminologie und Abgrenzung](#terminologie-und-abgrenzung)
- [Entwurf verteilter Datenbanken](#entwurf-verteilter-datenbanken)
- [Horizontale und vertikale Fragmentierung](#horizontale-und-vertikale-fragmentierung)
- [Fragmentierung nach Bundesland und Kundenadresse/Kontodaten](#fragmentierung-nach-bundesland-und-kundenadresse-kontodaten)
- [Grundlegende Korrektheitsanforderungen an eine Fragmentierung](#grundlegende-korrektheitsanforderungen-an-eine-fragmentierung)
- [Join-Auswertung in VDBMS](#join-auswertung-in-vdbms)
- [Join-Auswertung im VDBMS: Join-Auswertung mit Filter](#join-auswertung-im-vdbms--join-auswertung-mit-filter)
- [Transkationskontrolle in VDBMS](#transkationskontrolle-in-vdbms)
- [Mehrbenutzersynchronisation in VDBMS](#mehrbenutzersynchronisation-in-vdbms)
- [Deadlocks in VDBMS](#deadlocks-in-vdbms)
- [Deadlocks in VDBMS -- Timeout](#deadlocks-in-vdbms----timeout)
- [Zentralisierte Deadlock-Erkennung](#zentralisierte-deadlock-erkennung)
- [Dezentrale Deadlock-Erkennung](#dezentrale-deadlock-erkennung)
- [Deadlockvermeidung](#deadlockvermeidung)
- [Synchronisation bei replizierten Daten](#synchronisation-bei-replizierten-daten)
- [Arbeiten mit verteilten DBMS](#arbeiten-mit-verteilten-dbms)
- [ETL - Extract-Transform-Load](#etl---extract-transform-load)
- [Idee Block Chain](#idee-block-chain)
- [Anwendungsbeispiele](#anwendungsbeispiele)
- [Konsequenzen](#konsequenzen)


#### Verteilte DBMS
* Verteilte Datenbank
 * Verbund mehrerer logisch miteinander zusammenhängender Datenbanken, die jeweils auf verschiedenen Knoten eines Rechnernetzes liegen
* Verteiltes Datenbankmanagementsystem
 * Software-System zur Verwaltung einer verteilten Datenbank, wobei dem Benutzer die Verteilung der Daten (weitgehend) verborgen bleibt
* __Beispiel__:
 * Bank mit mehreren Filialen
 * Einzelnen Filialen sollen autonom Daten ihrer lokalen Kunden bearbeiten können
 * Gleichfalls sollten auch andere Filialen und Zentrale Zugriff auf diese Information haben
 * Lokale Stationen (Sites) über Kommunikationsnetz miteinander verbunden

#### Terminologie und Abgrenzung
* Kommunikationsmedium können unterschiedlichste Verbindungen sein:
 * LAN/WLAN/Telefonverbindungen (ISDN) oder einfache Modemverbindung

![vbdms](./pictures/vbdms.png)

* Client-Server-Architektur:
 * Nur Server speichert Daten ab
 * Klienten schicken Anforderungen an den Server und bearbeiten vom Server übermittelte Daten lokal
 * Ggf. wieder zurücksenden an Server
 * Datenverarbeitungsoperationen nur zusammen mit zentralem Server möglich

![vbdms2](./pictures/vbdms2.png)

#### Entwurf verteilter Datenbanken

![entwurf](./pictures/entwurf_vbdms.png)

>>>
* Fragmentierungsschema:
 * Relationen werden in (weitgehend) disjunkte Fragmente (Untereinheiten) zerlegt
 * Zerlegung auf Grundlage des Zugriffsverhaltens auf diese Fragmente
 * Umfassendes Wissen über zu erwartenden Anwendungen auf DB notwendig
 * Daten mit ähnlichem Zugriffsmuster -> in ein Fragment
>>>

![fragmente](./pictures/fragmente.png)

>>>
* Fragmentierungsschema festgelegt:
    * Zuordnung der Fragmente vornehmen -> Allokation
    * Zwei Arten d. Allokatoin
        * Redundanzfreie Allokation:
            * Jedes Fragment genau einer Station zugeordnet
            * Eine Station muss bevorzugt, eine benachteiligt werden
        * Allokation mit Replikation:
            * Allgemeiner Fall, N:M-Zuordnung, einige Fragmente repliziert/mehreren Stationen zugeordnet
            * Nur Leseanwendungen bevorteilt, da Änderungen auf alle Kopien müssen
>>>

![replikation](./pictures/allokation_replikation.png)

#### Horizontale und vertikale Fragmentierung
* Horizontale
 * Relation in disjunkte Tupelmengen zerlegt
* Vertikale
 * Attribute zusammengefasst
 * Relation vertikal durch Ausführung von Projektionen zerlegt
* Kombinierte
 * Beides...

#### Fragmentierung nach Bundesland und Kundenadresse/Kontodaten

![fragmentierung.](./pictures/horzvert.png)

#### Grundlegende Korrektheitsanforderungen an eine Fragmentierung
* Rekunstruierbarkeit
    * Die fragmentierte Relation lässt sich aus Fragmenten wiederherstellen
* Vollständigkeit
    * Jedes Datum ist einem Fragment zu geordnet
* Disjunktheit
    * Die Fragmente überlappen sich nicht, d.h. ein Datum nicht mehreren Fragmenten zugeordnet

>>>
#### Join-Auswertung in VDBMS
* Join-Operationen noch kritischer als in zentralen DBMS
* Problem: Beide Argumente können auf untersch. Stationen des VDBMS liegen
* -> Mehr oder weniger große Datenmengen müssen über Kommunikationsnetz transferiert werden
>>>

#### Join-Auswertung im VDBMS: Join-Auswertung mit Filter
* Join-Auswertung ohne Filter:
    * Unter Umständen sehr große Datenmengen zu transferieren trotz mögl. kleinem Endergebenis
* Join-Auswertung mit Filter:
    * Join-Auswertung mit Semijoin-Filterung
    * Join-Auswertung mit Bitmap-Filterung (Danke)

#### Transkationskontrolle in VDBMS
* Transaktionen können über mehrere Rechenknoten erstrecken
* Bsp: Überweisung von Konto A auf Station S<sub>A</sub> zu Konto B auf S<sub>B</sub>
* Beide Stationen lokal autonome DBMS -> Jeweils lokale Protokolleinträge
* Protokoll lokal für Redo und Undo benötigt#
* __Redo__:
    * Alle Änderungen einmal abgeschl. Transaktionen auf Daten d. Station müssen wiederhergestellt werden
* __Undo__:
    * Änderung noch nicht abgeschl. Transaktionen müssen rückgängig gemacht werden
* Redo/Undo nicht grundlegend anders als zentralisiert __außer:__
    * Nach Abbruch globaler Transaktion muss Undo-Behandlung auf allen Stationen initiiert werden die beteiligt waren
    

#### Mehrbenutzersynchronisation in VDBMS
* Erweiterung des Verfahrens im zentralen Fall
* Zwei-Phase-Sperrprotokoll
    * Jedes Objekt muss gesperrt werden
    * Eine Transaktion fordert Sperre an die sie schon besitzt nicht erneut an
    * Muss die Sperren anderer Transaktionen auf Objekt beachten
        * Wenn Sperre nicht gewährt -> Warteschlange
* Zwei-Phasen-Sperrprotokoll __zentralisiert:__
    * Wachstumsphase: Sperren anfordern aber nicht freigeben
    * Schrumpfphase: Bisherige Sperren freigeben, keine weiteren anfordern
    * Bei Transaktionsende: Alle Sperren zurückgeben
    
![Sperrung](./pictures/sperrung.png)

![Locks](./pictures/locks.png)

* Zwei-Phase-Sperrprotokoll __verteilt:__
    * Konzeptuell keine Änderung notwendig
    * Sperren dürfen nicht am Ende der Bearbeitungsphase an einer bestimmten Station freigegeben werden, sondern müssen alle bis Transatktionsende gehalten werden

#### Deadlocks in VDBMS
* Deadlockerkennung deutschlich schwieriger:
    * Timeout
    * Zentralisierte Deadlockerkennung
    * Dezentrale (verteilte) Deadlockerkennung

#### Deadlocks in VDBMS -- Timeout
* Nach versteichen v. Zeitintervall wird Deadlock angenommen
* Transaktion zurücksetzen und neustarten
* Wichtig: Wahl des Intervalls
    * zu lang: Schlechte Ausnutzung der Systemressourcen
    * zu kurz: Transaktionen die kein Deadlock sind zurückgesetzt

#### Zentralisierte Deadlock-Erkennung
* Stationen melden lokal vorliegende Wartebeziehungen an neutralen Knoten
* Baut globalen Wartegraph
* Zyklen -> Deadlock
* Nachteile:
    * Hoher Aufwand/Viele Nachrichten
    * Entstehung von Phantom-Deadlocks
        * Meldung zur Entfernung einer Wartebeziehung wird überholt von Meldung zum Neueintrag einer Wartebeziehung

#### Dezentrale Deadlock-Erkennung
* An einzelnen Stationen lokalen Wartegraph führen
* Lokale Deadlocks aufdecken
* globale Deadlocks aufdecken:
    * Lokaler Wartegraph hat Knoten "external" der stationenübergreifende Wartebeziehungen zu externen Subtransaktionen modelliert
    * Jeder Transaktion wird Heimatknoten zugeordnet
        * Von wo aus sog. externe Subtransaktionen auf andere Stationen initiiert werden

#### Deadlockvermeidung
* In VDBMS größere Bedeutung als in zentralisierten, da schwieriger aufzudecken
* Vermeidung:
    * Synchronisationsverfahren die nicht auf Sperren basieren
    * Bei sperrbasierter Synch. Blockierung einer Transaktion einschränken (ältere automatisch abbrechen bei Blockierung durch jüngere)
    * Alle Verfahren wie bei zentralisierten DBMS (wow)

>>>
#### Synchronisation bei replizierten Daten
* Zusätzliches Problem wenn in VDBMS Daten repliziert wurden -> kein Problem wenn nur Lesezugriff
* Änderung der Daten:
    * Alle bestehenden Kopien müssen geändert werden
    * Schreibsperren auf alle Kopien erwerben
    * Station mit zu ändernder Kopie nicht verfügbar: Warten oder abbrechen
    * -> Hohe Laufzeit und Verfügbarkeitsprobleme
>>>

#### Arbeiten mit verteilten DBMS
* Änderung an Datenmodellen in einzelnen Systemen erzeugen hohen Anpassungsaufwand, da Prog. die über APIs zugreifen jeweils angepasst werden müssen
* "Master Data Management" ist __DIE__ Herausforderung für grosse Unternehmen (*gähn)
* Lösung sind ETL Systeme (INFORMATICA, DELL BOOMI...)

#### ETL - Extract-Transform-Load
* Extract
    * Extraktion v. Datenausschnitt aus Quellsystem
    * Schema Transformation
    * Extraktion -> periodisch/ereignisgesteuert/anfragesteuert
* Transform
    * Syntaktische Transformation (Datentypen, Syntax,...)
    * Semantische Transformation
        * Eleminierung von Duplikaten
        * Anpassung von Datenwerten
        * Umrechnung von Masseeinheiten
        * Aggregation
        * Anreichrung der Daten
* Load
    * Übertragng der Daten ins Zielsystem

>>>
#### Idee Block Chain
* Datensätze in Blöcke verteilt und verschl. abgespeichert
* Verschlüsselungsalgo basiert auf Vorgängerblock
* Spätere Transaktionen bestätigen frühere Transaktionen
* Korrektheit v. Block immer durch Vorgängerblock prüfbar
* BLocks im Netz verteilt auf Servern gespeichert
* Bei Korrumpierung eines Blocks -> Korrektheit der Folgeblöcke zerstört
* In einem Block:
    * Krypto. sicherer Hash
    * Zeitstempel
    * Transaktionsdaten
* Ähnlich wie Buchführung die immer fortgeschrieben wird
![Blockchain](./pictures/blockchain.png)
 
#### Anwendungsbeispiele
* Bitcoin, Konten, Verteilte Texte, Grundbücher, Verträge, Große Geldtransaktionen, Wasserzeichen.........
#### Konsequenzen
* Disruptive Technology! *__BUZZWORDS__*
* Netz stellt Korrektheit sicher
* Notwendigkeit von sicherheitsgarantierenden Einrichtungen verschwindet
>>>
