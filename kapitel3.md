- [Pufferverwaltung](#pufferverwaltung)
- [Puffer](#puffer)
- [Suchen einer Seite](#suchen-einer-seite)
- [Speicherzuteilung im Puffer](#speicherzuteilung-im-puffer)
- [Lokale Strategien zur Speicherzuteilung](#lokale-strategien-zur-speicherzuteilung)
- [Globale Strategien zur Speicherzutielung](#globale-strategien-zur-speicherzutielung)
- [Seitentypbezogene Strategien](#seitentypbezogene-strategien)
- [Ablauf von Seitenersetzungs](#ablauf-von-seitenersetzungs)
- [Laden der Seiten vom Sekundärspeicher](#laden-der-seiten-vom-sekund-rspeicher)
- [Mermale gängiger Strategien](#mermale-g-ngiger-strategien)
- [FIFO Strategie](#fifo-strategie)
- [LFU-Strategie](#lfu-strategie)
- [LRU-Strategie](#lru-strategie)
- [Andere Strategien](#andere-strategien)
- [Fazit und Tuning des Puffers](#fazit-und-tuning-des-puffers)

---

#### Pufferverwaltung

* Reduzieren der Zugriffslücke
* Aufgaben: Lokalität d. Zugriffe ausnutzen und Daten (Hintergrundspeicher) zur Verarbeitung im Hauptspeicher verwhalten
* Unter: 
 * Nutzung effizienter Sucherverfahren
 * Geschickter Speicherzuteilung (Versch. Transaktionen wollen parallel Seiten)
 * Seitenersetzungsstrategien

#### Puffer

* Bereich des Hauptspeichers
* In Rahmen gegliedert
* Rahmen kann eine Seite der Platte aufnehmen/zwischenspeichern

#### Suchen einer Seite
* Seite angefordert -> Pufferverwaltung prüft ob im Puffer vorhanden
    * Ja: aus Puffer bereitstellen

*  __direkte__ Suche:
    * ohne Hilfmittel linear suchen (Alle Rahmen auf Seitennummer prüfen)
    * Ineffizient
*  __indirekte__ Suche:
    * Suchen über Zusatzstrukturen: Seitenlisten (Liste von Seiten im Puffer)
    * __unsortierte__ Tabelle:
        * In Reihenfolge der Pufferrahmen gespeichert
        * Muss voll durchsucht werden 
    * __sortierte__ Tabelle:
        * Einträge nach Seitennummer sortiert
        * schneller...
        * Änderungsaufwand höher, sortierreihenfolge muss eingehalten werden

#### Speicherzuteilung im Puffer
* Mehrere Transaktionen parallel: Puffer aufteilen unter Transaktionen
* Vergabe v. Seiten abhängig von freier Rahmenzahl/lokale Referenzverhalten d. Transaktionen

* Mehrere Strategien:
 * Lokale
 * Globale
 * Seitentypbezogene

#### Lokale Strategien zur Speicherzuteilung
* Jede Transaktion bekommt bestimmte Pufferteile
* Puffer in disjunkte Bereiche eingeteilt
* Größe der Bereiche:
 * Statisch: Vor Ablauf der Transkation entschieden
 * Dynamisch: Zur Programmlaufzeit enschieden

#### Globale Strategien zur Speicherzutielung
* Zugriffsverhalten aller Transaktionen berücksichtigt
* Bessere Berücksichtigung von Seiten die gemeinsam von mehreren Transaktionen referenziert

#### Seitentypbezogene Strategien
* Partitionen des Puffers in mehrere Typen von Pufferrahmen
* Pufferrahmen für:
 * Datenseiten
 * Zugriffspfadseiten
 * Data-Dictionary-Seiten
 * __UND WEITERE TYPEN WOW WIE GENAU DANKE MATTHIAS__

#### Ablauf von Seitenersetzungs
* Speicher fordert Seite an die nicht in Puffer
* Alle Pufferrahmen belegt
* DA MUSS WAS RAUS!!
* Seite raussuchen und wegwerfen
 * Wenn Seite verändert: zurückschreiben
 * Wenn Seite gleich: Einfach drüber mit der neuen

#### Laden der Seiten vom Sekundärspeicher
* Demand-paging-Verfahren (Standard)
 * Genau eine Seite im Puffer durch angeforderte ersetzt
* Prefetching
 * auch weitere evtl. benötigte werden eingelesen

#### Mermale gängiger Strategien
* Kennzahlen benötigt um Referenzierungsverhalten zu charakterisieren
* Diese führen zu Indizien für Wichtigkeit

*  *__Messbare Kennzahlen:__*
    * Alter der Seite im Puffer
        * Nach Einlagerung/Letztem Referenzzeitpunkt/Nicht berücksichtigt
    * Anzahl der Referenzen auf Seite in puffer:
        * Anzahl aller/der letzten Referenzen oder einfach nicht berücksichtigen

#### FIFO Strategie
* First In, First Out.
* Seite die am längsten im Puffer -> Weg

#### LFU-Strategie
* Least Frequently Used
* Am seltensten referenzierte ersetzt
* Kann passieren dass sehr alte Seite die vor langer Zeit oft referenziert nicht mehr ersetzt wird
* ALSO: ALTER miteinbeziehen. Das führt uns zu *trommelwirbel*

#### LRU-Strategie
* Least Recently Used
* Am längsten nicht referenziert wird ersetzt
* Gestamzahl der Referenzen ist also Wurst, nur Verhalten in jüngster Zeit
* Wenn referenziert -> Oben auf den Stack

#### Andere Strategien
* DGCLOCK
 * Kombiniert Alter und Referenzhäufigkeit (Vgl. Vorlesung OS)
* Adaptive Verfahren
 * ARC. Sowohl Alter Als auch Häufigkeit berücksichtigt
 * Mehr Infos? Nein, nicht in dieser Vorlesung. Fuck off.

#### Fazit und Tuning des Puffers
* Ziel: 90% Seitenzugriffe aus Puffer
* Nur in klassischen Anwendungen leicht erreichbar, sonst Puffergröße erhöhen
* Kommerzielle DBMS haben Werkzeuge für Konfiguration/Optimierung der Pufferverwaltung
