#### DB Administration
Warum???

- [DB Administration](#db-administration)
- [Datenbank Rollen](#datenbank-rollen)
- [Der Datenbank-Administrator](#der-datenbank-administrator)
- [Data Dictionary](#data-dictionary)
- [Tablespaces](#tablespaces)

***

#### Datenbank Rollen
* Datenbank Administration
* Security Officer
* Netzwerk Administrator
* Application Developer
* Application Administrator
* Database User
* Der Typ der das Firmenrepo forked und 2 Monate an allen anderen vorbei entwickelt

#### Der Datenbank-Administrator
* Installiert DB-Server und Tools
* Plant und allokiert System-Speicherplatz, Datenfiles(Tablespaces) in Abstimmung mit Application Developer
* Legt Tabellen, Indizes, Views mit Application Developer an
* Optimiert Strukturen (Seteifüllgrade, Komprimierung)
* Legt User an, überwacht Userzugriffe
* Stellt Lizenzvergabe sicher
* Plant Backups/Restores
* Archiviert Datenbank auf Tape
* Schnittstelle zum DBMS Hersteller bei technischen Fragen
* wahrscheinlich der langweiligste Mensch im ganzen Laden

#### Data Dictionary
*  __Enthält__ alle Informationen über komplette schema
* Speicher/Platverwaltung
* Rechetevergabe
* Defaultwerte und Integritätsbedingungen
* Statistics
* Drei Punkte, diese stehen für mehr Krempel. Ganz wichtig

* Ist __Read-Only__
* Strukturiert in Tables/Views
* Liegt in Haupspeicher/Cache
* Bei jedem DDL Statement geändert (Von DBMS man, das ist read only)
* Verfügt meist über User tabelle/views, DBA Tabelle/Views und "wichtige" Tabellen wie:
 * User_Tables
 * User_Tab_Colums
 * User_views
 * *gähn*
 * User_tablespaces
 * user_tab_histrograms
 * User_tab_statistics

#### Tablespaces

* Diese Folien sind nichtssagend. Danke.
* Größte logische Speichereinheit
* Vorstellbar wie Partition einer Festplatte, die ausschließich für Daten einer Datenbank reserviert
* Auf OS Ebene ist Tablespace ein oder mehrere Dateien fester Größe


![Ein Hund](./pictures/hunt.jpg)
