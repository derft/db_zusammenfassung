- [Kapitel 2 - Hintergrundspeicher](#kapitel-2---hintergrundspeicher)
    + [Verwaltung des Hintergrundspeichers](#verwaltung-des-hintergrundspeichers)
    + [Zwecke von Speichermedien](#zwecke-von-speichermedien)
    + [Speicherhierarchie](#speicherhierarchie)
    + [Magnetbänder](#magnetb-nder)
    + [Speicherpyramide](#speicherpyramide)
    + [Cache-Hierarchie](#cache-hierarchie)
    + [Zugriffslücken in Zahlen](#zugriffsl-cken-in-zahlen)
    + [Lokalität des Zugriffs](#lokalit-t-des-zugriffs)
    + [Die Magnetplatte](#die-magnetplatte)
  * [Lokalität ausnutzen](#lokalit-t-ausnutzen)
  * [Die Magnetplatte - Der Controller](#die-magnetplatte---der-controller)
    + [Flash-Laufwerke](#flash-laufwerke)
    + [Beheben der Probleme bei Flash-Laufwerken](#beheben-der-probleme-bei-flash-laufwerken)
    + [Konsequenzen: Einsatz Flash in DBMS](#konsequenzen--einsatz-flash-in-dbms)
    + [Typische Anwendungsfälle von Flash:](#typische-anwendungsf-lle-von-flash-)
    + [Speicherarrays: RAID](#speicherarrays--raid)
    + [Möglichkeiten der Datenverteilung](#m-glichkeiten-der-datenverteilung)
    + [Möglichkeiten der Datenverteilung - DB Design](#m-glichkeiten-der-datenverteilung---db-design)
    + [Speicherarrays : RAID](#speicherarrays---raid)
    + [Erhöhung der Feletoleranz durch Redundanz](#erh-hung-der-feletoleranz-durch-redundanz)
    + [Erhöhung der Effizienz durch Parallelität d. Zugriffs](#erh-hung-der-effizienz-durch-parallelit-t-d-zugriffs)
    + [RAID-Levels](#raid-levels)
      - [Folgende RAID Levels kombineren Paritybity / ECC (statt Spiegelung) und Striping](#folgende-raid-levels-kombineren-paritybity---ecc--statt-spiegelung--und-striping)
    + [Tertiärspeicher](#terti-rspeicher)
    + [Langzeitarchivierung](#langzeitarchivierung)
    + [Organisation d. physischen Informationen in Form von physischen Dateien](#organisation-d-physischen-informationen-in-form-von-physischen-dateien)
    + [Blöcke und Seiten](#bl-cke-und-seiten)
    + [Einpassen von Datensätzen auf Blöcke](#einpassen-von-datens-tzen-auf-bl-cke)
    + [Blockungstechniken](#blockungstechniken)
    + [Struktur der Seiten](#struktur-der-seiten)
    + [Klassifikation v. Datensätzen](#klassifikation-v-datens-tzen)
    + [Sätze fester Länge](#s-tze-fester-l-nge)
    + [Sätze variabler Länge](#s-tze-variabler-l-nge)
    + [Adressierung von Datensätzen](#adressierung-von-datens-tzen)
    + [TID-Konzept](#tid-konzept)
    + [Kompression von Daten](#kompression-von-daten)
    + [Run Length Encoding](#run-length-encoding)
    + [Delta Coding](#delta-coding)
    + [Bit-Vector Encoding](#bit-vector-encoding)
    + [Dictionary Encoding](#dictionary-encoding)
    + [Overview Seitenstruktur](#overview-seitenstruktur)
---

## Kapitel 2 - Hintergrundspeicher

#### Verwaltung des Hintergrundspeichers
* Daten auf versch. Speichermedien (Platten/Bänder) gespeichert
* Diese Medien wieder in Blöcke/Seiten/Sätze strukturiert
* Zur Verarbeitung: Daten in den Hauptspeicher übertragen
 * Dort Daten im sog. Puffer verwaltet und mögl. lang zwischengespeichert
 * -> schneller Zugriff bei weiteren Verarbeitungsschritten

#### Zwecke von Speichermedien
* Daten bereitstellen
* Daten langfristig speichern (Dennoch schnell zur Verfügung halten)
* Daten sehr langfristig und preiswert archivieren (Inkaufnahme längerer Zugriffszeiten)

#### Speicherhierarchie
* Extrem schneller Prozessor
* Sehr schnell Cache-Speicher
* Schneller Hauptspeicher

* Langsamer Sekundärspeicher

* Sehr langsamer Nearline-Tertiärspeicher 
 * autom. bereitgestellt
* Extrem langsamer Offline-Tertiärspeicher
 * manuell bereitgestellt (Schränke / Laggerraum)

#### Magnetbänder
* Früher allgemein weit verbr. Technik zur Datenspeicherung
* Im Privatbereich nur noch selten eingesetzt
 * Kosten, umständlich
* Im prof. Umfeld nach wie vor häufig eingesetzt
 * allgemeine hohe Zuverlässigkeit und Langzeitstabilität

#### Speicherpyramide

![Speicherpyramide](./pictures/speicherpyramide.png)

#### Cache-Hierarchie
* Zugriffslücke: Unterschiede zwischen Zugriffsgeschw. auf die Daten
* Cache Speicher:
 * Speichert auf Eben _x_ Daten von Ebene _x+1_ zwischen
 * vermeidez Zugriffslücken zw. einzelnen Ebenen
* Verschiedene Arten von Caches:
 * Cache (Hauptspeicher-Cache):
  * stellt Hauptspeicherinhalte mit schneller Technologie für Proz. bereit
  * eigene Ebene in Speicherhierarchie
 * Plattenspeicher-Cache im Hauptspeicher (Puffer):
  * speichert Ausschnitte des Sekundärspeichers zwischen
 * WWWW-Cache
  * Cache beim Zugriff auf Daten im WWW über HTTP
  * Teil v. Plattenspeicher

#### Zugriffslücken in Zahlen
 
![Zugriffslücken](./pictures/zugriffsluecken.png)

#### Lokalität des Zugriffs
* Caching-Prinzip funktioniert nicht bei immer neuen Daten
* Caching-Prinzip funktioniert nur wenn Lokalität ausnutzbar
* Deshalb Pufferverwaltung wichtiges Konzept
* __Zeitliche Lokalität:__
 * In kuzer Zeit wiederholt auf gleichen Daten zugegriffen
 * Großteil d. Zugriffe können mit Daten aus Cache beantwortet werden
* __Räumliche Lokalität:__
 * Zusamen angefragte Daten sind auf dem Hintergrundspeicher zusammen abgel.
 * Anzahl der IO-Operationen werden reduziert

#### Die Magnetplatte

![Magnetplatte](./pictures/magnetplatte.png)

*  __Die Aufzeichnungskomponente (Die ganze Lila-Kacke da):__
 * Plattenstapel (Mit Platten durrrr)
 * Rotation bis zu 15k rpm
 * Für jede Plattenoberfläche (2x Anzahl Plattten) Schreib/Lebekopf
 * Oberfläche: Konzentrische Kreise (Spuren)
 * Übereinander angeordnete Spuren aller Plattenoberflächen ergeben Zylinder
 * Spuren: bestehen aus Sektoren, größte von Hersteller festgel.
 * Sektor enthält Daten zur Selbstkorrektur -> Paritätsbits oder __E__rror __C__orrecting __C__odes
 * Kleinste Zugriffseinheit: Block
 
* __Die Positionierungskomponente (Der gelbe Bolzen):__
 * Adressieung von Blöcken geschieht über Zylinder, Spur und Sektornummer
 * Kamm zum Zylinder -> auf Sektor warten der vorbeirotiert -> Blockinhalt lesen und übertragen

### Lokalität ausnutzen
*  __Clustering:__
 * Daten auf der Platte in einer günstigen Reihenfolge anordner
 * -> Zeitvorteil d. sequenziellen Zugriffs / räumliche Lokalität ausnutzen
* __Prefetching:__
 * noch nicht benötigte aber wahrsch. in Kürze benötigte (und in seq. Reiehnfolge leicht erreichbare) Daten mitlesen

### Die Magnetplatte - Der Controller
* Enthält eigenen Mikroproz. und eigenen Cache
* sorgt für Abbildung d. Speicheradresse auf physischen Sektor d. Platte
* Übertragungsrate bis zu 300 MB/s

#### Flash-Laufwerke

* Festkörperlaufwerke (SSD)
* Ohne beweg. mechanische Teile
* Schneller als Magnetplattenlaufwerke
* Weniger Energieverbrauch als M.
* Teurer als HDD, günstiger als SDRAM
* __Aufbau:__
 * Daten in Flash-Blocks (Speicherzellenarrays)
 * Jeder Block meist 128KB, unterteilt in 2KB große Seiten
 * Initialer Status Zelle: 1, kann auf 0 prog. werden
 * Löschen: Zelle = 1
 * Nur ganze Blocks löschbar
* __Vorteil:__
 * Keine Mech. Teile -> schneller wahlfreier Zugriff (Speicherzugriff auf bel. Element in konstanter Zeit)
* __Nachteil:__
 * Löschoperation langsam. Lesen 25µs, Löschen 2ms
 * Lebensdauer d. Zellen begrenzt
 * Anzahl Lösch-Schreibzyklen meist < 100k Zyklen

#### Beheben der Probleme bei Flash-Laufwerken
*  __begrenzte Lebensdauer:__
 * Schreibvorgänge gleichmäßig auf alle Blöcke vert. (Wear Leveling)
 * -> Zuordnung physik. Speicheradresse nur von SSD gespeichert, von außen nicht einsichtlich
 * Defragmentierung kontraproduktiv

#### Konsequenzen: Einsatz Flash in DBMS
* Klassische DBMS-Impl.: ausger. auf sequentielles Lesen, nicht wahlfrei
 * Stärken von Flash nicht ausgenutzt
* Obwohl kleinere Blockgrößen effizient verarbeitbar, Wear Leveling -> Blockgrößten 32KB
* Wahlfreie Lesezugr. auf kleine Ergebnismenngen (bis 16MB) zu begrenzen
* __Einfaches Ersetzen führt nicht zu Leistungssteigerung__

#### Typische Anwendungsfälle von Flash:
* schnellere Festplatte
 * Erfordert Anpassung der Datenstruktur
* Zusätlicher Cache zw. Hauptspeicher und Magnetplatte
 * Lesen schneller
 * Kosten gegenüb. reinem SSD-Einsatz bleiben gering
 * Schreibperformance gleich
* Medium für Spezialzwecke
 * SSD für Log/Journal (Protokoll und Rücksetzen von Transaktionen (Recovery))

#### Speicherarrays: RAID
* Zugriffszeiten v. Magnetplatten nicht in großem Umfang steigerbar
* Schutz vor Fehlern -> Duplikate auf der Platte
* RAID: Redundant Array of Independent Disks

#### Möglichkeiten der Datenverteilung
1. Mittels RAID -> erscheint als logische Platte (gespiegelt oder gestriped)
2. Datencontainer (Tablespaces bspw) auf Platten verteilt
3. Tabelle auf Datencontainer verteilt
4. Datensätze auf Tabellen verteilen

#### Möglichkeiten der Datenverteilung - DB Design
* Überlegen weiviele Platten / welches RAID Level
* Lege Tabellen und Index Design fest
* Lege Contrainer-Struktur fest
* Balance Logik,Sicherheit bla bla bla 

#### Speicherarrays : RAID
* billige Magnetplatten unter Controller zu einem Laufwerke
* Wie einzelne Festplatte v. außen
* Controller verteilt auf Platten
*  __Ziele:__
 * Fehlertoleranz
 * Effizienzsteigerung

#### Erhöhung der Feletoleranz durch Redundanz
* Zusätzliche Platten -> Speicherung von Duplikaten (Spiegeln), bei Fehler: Umschalten auf Spiegelplatte
 * Raid 1, 0+1
* Alternative: Kontrollinformationen (Paritätsbits) nicht im selben Sektor sondern auf anderer Platte
 * Raid Level 2-6 stellen durch Parity-Bits / ECC Daten wieder her

#### Erhöhung der Effizienz durch Parallelität d. Zugriffs
* Datenbank auf mehrere Platten verteilen -> Parallelisierung d. Zugriffs
 * Zugfriffszeit verringert fast linear mit Anzahl d. Platten
 
* Verteilung d. Daten auf Platten:
 * bitweise
 * byteweise
 * blockweise

#### RAID-Levels
* Unterscheiden sich in Art d. Datenvertailung und der Art d. Redundanz/Fehlerkorrektur

*  __RAID Level 0:__
 * Blöcke im Rotationsprinzip auf Platten verteilt (Striping, hier blockweise)
 * Block _i_ also immer auf Platte _i mod n_ von _n_ Platten gespeichert

*  __Vorteile RAID 0:__
 * Daten aufeinanderfolgender Blöcke parallel lesbar
 * Lastbalancierung bei vielen parallel anstehenden Lesetransaktionen (ben. Blöcke wahrsch. auf versch. Pltten)
 
*  __Nachteile RAID 0:__
 * Keine Beschleunigung d. wahlfreien Zugriffs auf genau 1 Block
 * Effizienzsteigerung auf Kosten der Fehlertoleranz (Keine Redundanz / Fehlerkorrektur)
 
*  __RAID Level 1:__
 * Stezt voll auf Redundanz
 * Blöcke auf mehrere Platten gespiegelt (Drive Mirroring)
 
*  __Vorteil RAID Level 1:__
 * Fehler auf Platte -> Umschalten auf Spiegelplatte

*  __Nachteil RAID Level 1:__
 * einzige Effizienzsteigerung: Lastbalancierung b. Lesen v. parallel anstehenden Transaktionen  (Daten v. Untersch. Spiegelplatten)
 * Verdopplung Speicherplatzbedarf

*  __RAID Level 0+1:__
 * Kombination soll Vorteile beider Levels vereinigen
 * Blöcke auf versch. Platten gestriped -> dann auf versch. Platten gespiegelt

##### Folgende RAID Levels kombineren Paritybity / ECC (statt Spiegelung) und Striping

*  __RAID Level 2:__
 * Striping auf Bitebene (Hamming System)
 * Paritybits/ erweiterte ECC auf zus. Platten
 * _i_-te Bit auf Platte _i mod n_ von _n_ Platten gespeichert
 * Kontrollinformationen auf zusätz. Platten
 * Einlesen: mehrere parallele Leseoperationen zur Rekonstruktion v. Byte
 * Zugriffe nicht effizienter
 
*  __Paritätsbit__ zeigt Fehler an aber nicht wo
 * Bsp Even-Parity: Das Informationswort 0011.1010 hat vier Einsen. Vier ist eine gerade Zahl, das Paritätskontrollbit ist also die Null, und das resultierende Codewort ist 0011.1010 0.
 * Das Informationswort 1010.0100 hat hingegen eine ungerade Paritätssumme und wird in das Codewort 1010.0100 1 codiert.

*  __Error Correction Codes:__
 * Acht Bits -> 4 Fehlerkorrektur Bits
 * Nutzdaten: 0 0 1 1 0 0 1 0
 * Zu Übertragen: 0 0 1 1 ? 0 0 1 ? 0 ? ?
 * ? -> Korrekturbits, immer an 2erPotenzstellen (1,2,4,8,16,32..)
 * Jeder Stelle ein Wert, bei vier Bits:
  * 1: 0001
  * 2: 0010
  * 3: 0011 etc
 * Überall wo 1 in übertragung -> XOR, hier also stellen 5, 9 und 10
 * 0101 XOR 1001 XOR 1010 -> 0110
 * Dies in die ? Füllen
 * Empfangene Korrektbits XOR Berechnete Empfangsbits ? Korrekt : Falsch
 * XOR Wert gibt Stelle des falschen Bits an
 
* Gibt weitere Varianten, inkl. proprietäre und Kombinationen aus genannten

![Raids](./pictures/raids.png)

*  __Vor/Nachteile d. Levels:__
 * 0: Effizienzsteigerung, keine Ausfallsicherheit -> bspw Videoserver
 * 1: schnelle Fehlerkorrektur (gut für Log-Dateien)
 * 3 und 5: Verbesserung von 2 und 4...
 * 6: Verbesserung von 5 aber selten umgesetzt

#### Tertiärspeicher
* optische Platten oder Magnetbänder
* offline: Medien manuell ausgewechselt (optische Platten)
* nearline: Automatisch ausgewechselt (Magnetbänder, Jukeboxes, Bandroboter)

*  __optische Platten:__
 * Werden über Laserstrahl gelesen
 * relativ preiswert, langsamer wahlfreier Zugriff, ~30 Jahre relativ lange Lebensdauer
*  __Magnetbänder:__
 * sehr billig, preiswerte Laufwerke
 * Noch langsamerer zugriff
 * Wahlfreier Zugriff: Minutenbereich

#### Langzeitarchivierung
* Aspekte der Lebensdauer:
 * Physische Haltbarkeit -> __Unversehrtheit__
 * Vorhandensein v. Geräten und Treibern -> __Lesbarkeit__
 * Metadaten -> __Interpretierbarkeit__
 * Programmen die auf Daten arbeiten können -> __Wiederverwendbarkeit__

#### Organisation d. physischen Informationen in Form von physischen Dateien
* Verschiedene DBMS unterscheiden sich darin in wie weit sie sich auf Konzepte des OS stützen
 * DBMS kann jede Relation / jeden Zugriffspfad in einer OS-Datei speichern
  * Feinstruktur der DB im OS sichtbar
 * DBMS legt eine/mehrere Dateien an (durch OS), verwaltet Relationen/Zugriffspfade Selbstkorrektur
  * Feinstruktur nicht mehr sichtbar
 * DBMS steuert selbst Magnetplatte an und arbeitet mit Blöcken in Ursprungsform
  * Dateisystem an OS vorbei realisiert
* Warum nicht immer OS-Verwaltung 
 * Je mehr durch DBMS verwaltet desto Platformunabhängiger
 * OS unterstützt nur Daten auf max 1 Medium
 * OS-Puffer genügt nicht Anforderungen d. DB

#### Blöcke und Seiten
* physische Blöcke zu Seiten zusammengefasst (meist feste Faktoren, 1,2,4,8 Blöcke pro Seite)
* Höhere Schichten -> Seitennummeradressierung, erst "unten" Blocknummern

#### Einpassen von Datensätzen auf Blöcke
* Blocken: Datensätze in die Blöcke einpassen
 * Abhängig von variabler oder fester Feldlänger d. Datenfelder
  * Datensatz variable Länge: Hoher Verwaltungsaufwand b. Lesen und Schreiben, Satzlänge neu ermitteln
  * Datensatz feste Länge: höherer Speicheraufwand

#### Blockungstechniken
* Nicht-Spannsatz: Datensatz in max. 1 Block
* Spannsatz: Datensatz evtl. in mehreren Blöcken
 * Datensatzrest in neuen Block klatschen
* Standard: Nichtspannsätze

#### Struktur der Seiten
* Neue Daten werden eingetragen:
 * DBMS holt neue Seiten v. Freispeicherverwaltung
 * Freispeicherverwaltung führt Liste d. freien Seiten: Free-List
 * Free-List meist doppelt-verkettete Liste
 * Neue Seite wird benötigt für Datei -> wird an letzte bisher benutzte Seite d. Datei angehängt

__*Siehe Folien 115 bis 120 für Beispiel...*__

![Seitenstruktur](./pictures/seiten.png)

#### Klassifikation v. Datensätzen

![Datensatzklassifikation](./pictures/datensaz_klassifikation.png)

* Fixierte Sätze
 * An Position gebunden (Datensatz(115,142))
 * Löschung/Verschieben führt zu Anpassung aller Verweise auf Datensatz
* Unfixierte Sätze
 * Verweis durch logische Zeiger aus zentraler Stelle die auf aktuelle Adresse umsetzt (indexdatei)
 * Nachteil: Verschieben benötigt Laden der beiden betroffenen Seiten __und__ Indexseite

#### Sätze fester Länge
* Falls alle Felder v. Datensatz feste Länge:
 1. Verwaltungsblock mit Typ d. Satzes und Löschbit (kennz. ob aktuell oder löschbar)
 2. Freiraum -- folgender Bereich beginnt mit Offset
 3. Nutzdaten

#### Sätze variabler Länge

![Strategie a](./pictures/strat_a.png)

![Strategie b](./pictures/strat_b.png)

* Vorteil b: leichtere Navigation innerhalb d. Satzes
* Jedoch höherer Verwaltungsaufwand b. Lesen/Schreiben

#### Adressierung von Datensätzen
* Adressierung mit Angabe: Seitennummer, Offset auf Seite 
 * Seitennummer besteht selbst aus Speichermedium, Zylinder, Spur und Blocknummer
 * -> Fixierte Datensätze
* Lösung: TID

#### TID-Konzept
* TID = Tupelidentifikator
* TID Datensatzadresse bestehend aus: Seitenummer und Offset
* Offset verweist nicht auf Datensatz selbst sondern _i_-ten Eintrag in Liste von zeigern am Anfang d. Seite
* Verschiebung von Datensätzen auf Seite
 * Nur Offsetwert muss geändert werden
* Verschieben auf andere Seite
 * An pos des alten Datensatzes neuer TID-Zeiger
 * Dieser verweist auf Tupel Zeiger auf neuer Seite

#### Kompression von Daten
* Kosten sparen
* Mehr Seiten in Puffer
* __Anforderungen:__
 * Verlustlose Kompression
 * Dekompression leichtgewichtig
* __Kriterien:__
 * Kodierung
  * Feste Codelänge: alle werte mit gleicher Anzahl Bits codiert
  * Variable Codelänge: Häufige Werte mit weniger Bits dargestellt
 * Granularität d. Kompression
  * Attributwerte
  * Tupel
  * Spalten oder Tabellen
 * Umgang mit kompr. Daten
  * Beim lesen dekomprimieren -> normal weiterarbeiten
  * Direkte Ausfürhung von Operationen auf kompr. Datenstrukturen

#### Run Length Encoding
* Folgende gleiche Werte durch Eintrag der Anzahl codiert
* Kein Vorteil b. ständig wechselnden Daten
* Sortierung kann unterstützen

![RLE](./pictures/runlength.png)

#### Delta Coding
* Bei geringer Diff. aufeinanderfolgender Werte
* nur Delta gespeichert

![Delta-Coding](./pictures/delta.png)
* Sortierung hilfreich, aufwendigere Dekodierung

#### Bit-Vector Encoding
* Anzahl versch. Werte einer Spalte klein
* Bitstring Mit Länge = Tupelanzahl

![Bitstring Coding](./pictures/bitstring.png)

#### Dictionary Encoding
* Einzelne Werte häufig vorhanden
* Werte in Tabelle (Dictionary) abgelegt und über Bitcode identifiziert

![Dictionary Coding](./pictures/dictionary.png)
* Große Speicherplatzeinsparung bei wenigen aber häufigen Werten
* Nachteil: Für Dekodierung ist ein Lookup im Wörterbuch notwendig

#### Overview Seitenstruktur

![Seitenstruktur](./pictures/overview_seitenstruktur.png)
