- [Gründe für IMDB](#gr-nde-f-r-imdb)
- [Verteilter Hauptspeicherzugriff](#verteilter-hauptspeicherzugriff)
- [Partitionierung von Tabellen](#partitionierung-von-tabellen)
- [Vertikale/Horizontale Partitionierung](#vertikale-horizontale-partitionierung)
- [Round Robin Partitionierung](#round-robin-partitionierung)
- [Hash Partitionierung](#hash-partitionierung)
- [Applikations Partitionierung](#applikations-partitionierung)
- [Datenkompression](#datenkompression)
- [Prefix-Komprimierung](#prefix-komprimierung)
- [Run Length Komprimierung](#run-length-komprimierung)
- [Cluster Komprimierung](#cluster-komprimierung)
- [Verstreute (Sparse) Komprimierung](#verstreute--sparse--komprimierung)
- [Indirekte Komprimierung](#indirekte-komprimierung)
- [Delta Komprimierung](#delta-komprimierung)
- [Zusammenfassung Komprimierung](#zusammenfassung-komprimierung)
- [Ursasche Reaorganisation im Column Store](#ursasche-reaorganisation-im-column-store)
- [Vermeidung Reorg im Column Store](#vermeidung-reorg-im-column-store)
- [Verschmelzung Diffbuff mit Hauptspeicher](#verschmelzung-diffbuff-mit-hauptspeicher)
- [Delta Index](#delta-index)
- [Hauptspeicher Index](#hauptspeicher-index)
- [Aggregate](#aggregate)
- [Aggregat Cache](#aggregat-cache)
- [Aggregat Cache Auflösung](#aggregat-cache-aufl-sung)
- [Logging und Recovery](#logging-und-recovery)
- [Logging](#logging)
- [Dictionary Logging](#dictionary-logging)
- [Logging Beispiel](#logging-beispiel)
- [Recovery](#recovery)
- [Replikation](#replikation)
- [Transaktionswiederholung](#transaktionswiederholung)
- [Fail-Over](#fail-over)
- [Fail-Over-Steuerung](#fail-over-steuerung)
- [Data Aging](#data-aging)
- [Zusammenfassung Data Aging -- Wenn ich was lerne dann das... danke Matthias](#zusammenfassung-data-aging----wenn-ich-was-lerne-dann-das-danke-matthias)
- [Datenbank Reorganisation](#datenbank-reorganisation)
- [Multi-Tenancy](#multi-tenancy)
- [Multi-Tenancy Zusammenfassung](#multi-tenancy-zusammenfassung)

#### Gründe für IMDB
* Spezifische (Datenbank)-Applikationen in Unternehmen gehen unterschiedlich mit Datenerfassung/speicherung/verarbeitung um
    * -> sind entweder für Transaktionen oder Analyse optimiert
* __OLTP:__
    * Hohe Anzahl meist einfacher Adhoc Transaktionen
    * viele parallele Nutzer
    * Viele Schreib- Lesezugriffe
    * Rollenbasiert
    * Kurze Transaktionen
    * Kleine Ergebnismenge
* __OLAP:__
    * Weniger Transaktionen dafür komplexe Rechenoperationen
    * Weniger parallele Nutzer
    * Hauptsächlich Lesezugriffe meist für umfassende Berichte
    * Für bestimmte Nutzer (Sales Account Executive, Execute Board und weitere wichtig klingende Faulenzer)
    * Lange Transaktionen
    * Sehr grosse Ergebnismenge
* Hoher Synch. Aufwand zwischen Systemen, meist OLTP -> OLAP
    * Zeitverzug, oft nicht neuste Daten im OLAP-System
    * Redundanzen
    * Inkonsistenzen
    * Unterschiedliche Datenschemas
    * Erhöhte Kosten durch Beschaffung und Betrieb
    * Hoher Aufwand für Aggregate
    * Günstigere Hardware mit grossem Hauptspeicher

* Was spricht für Column Store?
    * Viele Spalten werden gar nicht genutzt (~50%)
    * viele Spalten haben niedrige Kardinalität (Land/Geschlecht)
    * Viele NULL / DEFAULT Werte
    * Niedriege statistische Verteilung der Dateninhalte unterstützt Kompression

    ![spalten](./pictures/spalten.png)

* OLAP/OLTP wirklich so unterscheiedlich?=
    * Analyse über viele Kundensysteme: Schreib- LEsezugriffe nicht signifikant unterschiedlich
    * Je nach Industrie Schreibzugriffe sogar sehr gering (Banking)
    
* Kundenwunsch: 
    * EIN System ohne Redundanzen und Inkonsistenzen
    
* Idee/Vision
    * Keine Aggregate/Views
    * alles in Hauptspeicher/Cache und zu Laufzeit berechnet

#### Verteilter Hauptspeicherzugriff
* Tabellen können über Knoten verteilt werden

![infiniband_vert](./pictures/infiniband_verteilung.png)

* Infiniband -> bidirektionaler serieller Bus, Latenzarm zur Verbindung von Haupspeicher mehrerer Cores

#### Partitionierung von Tabellen
* Parallele Ausnutzung mehrerer Core CPUs die auf Shared Memory zugreifen erhöhrt Verarbeitungsgeschwindigkeit signifikant
* Partitionierung legt Methode fest nach der Daten verteilt
* Kenntnis der Anfrage notwendig

#### Vertikale/Horizontale Partitionierung
* Siehe Andere Vorlesung, selber Unsinn
* Auftrennung der Tupel in Bereiche -> 5 Bereiche nach Kontinent, 200 nach Land... etc

#### Round Robin Partitionierung
![rrpart](./pictures/rrpart.png)

#### Hash Partitionierung
* Wähle Attribut als Hash-Kriterium
* Lege Anzahl Bits fest für Partitionierung (Bsp: 3 bits für 8 Partitionen)
* Verteile Daten gleichmäßig

#### Applikations Partitionierung
* Wähle Kriterium aus der Applikation zur Verteilung
    * Bspw Bestellzeitreum, Version des Artikels, ...

#### Datenkompression
* Für Attributvektoren
    * Prefix Verschlüsselung
    * Run Length Verschlüsselung
    * Cluster Verschlüsselung
    * Sparse Verschlüsselung
    * Indirekte Verschlüsselung
* Für Dictionary
    * Delta Kompression
    * Sortierte Arrays

#### Prefix-Komprimierung
* Ersparnis wenn eine Spalte mit lange Reihe selben Wertes beginnt __oder__
* bestimmter Wert häufig vorkommt und anderen seltener
* Wert nur ein mal gespeichert inkl. Anzahl der Vorkommen
* Voraussetung: Tabelle sortiert nach Spalte
* __Direkter Zugriff__ möglich

#### Run Length Komprimierung
* Ersetze Sequenz desselben Wertes mit einzelnen Instanz des Wertes und __Anzahl des Auftretens und Startposition__
* __Direkter Zugriff__ möglich

#### Cluster Komprimierung
* Attributvektor aufgeteilt in N Blöcke gleicher Größte (bspw 512 Byte)
* Wenn das Cluster immer nur einen Wert -> Wert nur ein mal
* Wenn das Cluster untersch. Werte -> 1:1 abgelegt
* Bitvektor zeigt an ob Fall 1 oder 2
* __Indirekter Zugriff__ nur über Bitvektor möglich

#### Verstreute (Sparse) Komprimierung
* Attributvektor wird nach häufigstem Wert durchsucht
* Bitvektor zeigt an an welcher Stelle Wert entfernt
* __Indirekter Zugriff__ nur über Bitvektor möglich

#### Indirekte Komprimierung
* Attributvektor aufgeteilt in N Blöcke gleicher Größte (bspw 512 Byte)
* Wenn Block viele verschiedene Einträge > Keine Aktion
* Wenn Block wenige unterschiedliche Einträge -> Block mit Dictionary Encoding komprimiert
* __Direkter Zugriff__ möglich

#### Delta Komprimierung
* Für Dictionary Tabelle
* Ergibt Sinn bei Sortierung
* Blockweise Komprimierung (Da hat er auf den Folien auch keinen Bock mehr)

#### Zusammenfassung Komprimierung
* Unterschiede zw. Attributvektor und Dictionary Komprimierung
* Meist sortierte Spalten nötig, Tabellen können nur nach __einer__ Spalte sortiert werden

#### Ursasche Reaorganisation im Column Store
* Einfügen/Ändern neuer Werte ins Dictionary
    * Reorg Dict -> Reorg Attributvektor
* Breite des Dictionary verlängert sich
    * Komplettumbau Dictionary -> Reorg Attributvektor
* Löschen einer Zeile (Ohne Dict Auswirkung)
    * Zusammenschieben des Attributvektors
* Einfügen einer Zeile (Ohne Dict Auswirkung)
    * Auseinanderziehen des Attributvektors

#### Vermeidung Reorg im Column Store
1. Datensätze nie löschen, Gültigkeit mit Zeitstempel kenntlich machen
    * Vorteil: Integrierte Historie
        * Automatisches Logging
        * Einfache Snapshots
        * Keine Reorgs mehr
    * Nachteil: Anstieg Speicherbedarf
    * Bspw "Gueltig_ab" Spalte, Insert/Update hat keinen effekt auf alte Tupel
    * SELECT muss letztes Tupel ausfindig machen -> Mit Bitvektor entschärfbar

2. Einführung von seperatorem Bereich der alle Änderung enthält -> Delta- / Differential Buffer
    * Lesen zuerst auf DiffBuff, wenn nicht vorhanden dann aus Hauptspeicher
    * Schreiben nur in DiffBuff

![diffbuff](./pictures/diffbuff.png)
    
    * Vorteil: schnelle Inserts, kein Reord oder Resort
    * Nachteil: 
        * Diffbuffer braucht zusätzlichen Speicher
        * Range SELECT über DiffBuff und Haupstpeicher aufwändiger/langsamer
        * Zusätzlicher Gültigkeitsvektor notwendig
        * Regelmäßiger Re-Merge von DiffBuff und Hauptspeicher nötig
        
![diffbuffbeispiel](./pictures/diffbuffbeispiel.png)

#### Verschmelzung Diffbuff mit Hauptspeicher
* Alle Schreiboperationen (INSERT UPDATE DELETE) immer erst in DiffBuff
* Veränderte Werte im Hauptspeicher mit Gültigkeitsvektor ungültig geflaggt (0 bit whatever)
* Immer nur gültige Datensätze auf Diffbuff und Haupspeicher gelesen
* Problem: Lesen auf Diffbuff __und__ Hauptspeicher verlangsamt Performance
* Abhilfe: Regelmäßiges Zurückschreiben des DiffBuff in den Hauptspeicher, sowohl Dict als auch Attributvektor
* Grundsätzlicher Ablauf:
    * Ungültiges in Hauptspeicher und Diffbuff in History File schreiben
    * Dicitonaries Kombinieren
    * Mapping von altem Diffbuff + Hauptspeicher Dictionary auf neues Dictionary
    * Aktualisieren des Attributvektors
    * Diffbuff auflöse und neue Hauptspeicher Attributvektor und Dictionary aufbauen

#### Delta Index
* Delta ist klein, braucht aber schnelle performance bei INSERT
* Wie schnell Wert zu Value ID oder umgekehrt finden?
* -> Nutzung von B+-Bäumen
    * Value ID = Knoten
    * Werte = Verweis auf sortiertes Dictionary
![Deltaindex](./pictures/deltaindex.png)

#### Hauptspeicher Index
* Hauptspeicher ist Read Only -> Nur für SELECT
* Wie finde ich schnell die Zeile mit best. Wert?
    * Finde bestimmten Wert in Sortiertem Dictionary (je nach Wert) (Bspw Berlin)
    * Finde RowID zu best. Wert (Bspw Berlin - Offset 1)
    * Hole Offset des nächsten Wertes (Bspw 3)
    * Suche in diesem intervall (1 <= x <3 ) (Ergebnis bspw RecID 4 und 6)
    * Hole nun Rowids (Bspw Rowids 4 und 6)
* GANZ TOLLE FOLIEN

#### Aggregate
* Arbeiten auf Datenmengen und nicht auf einzelnen Records
* mit HAVING kann Datenmenge noch mal eingeschr. werden
* Weitere Folien verstehe ich nicht :-)

#### Aggregat Cache
* In RDBMS Aggregate auf untersch. Weise vorgehalten
    * OLAP: Materialisierte Views
    * OLTP: Applikation hält interne Tabellen oder Wertebereiche vor
* Nachteile:
    * Applikationslogik wird komplizierter
    * Verlangsamung bei Schreiboperationen
    * Erzeugung von Redundanzen
    * Unflexible Analyse, da bei neuen Abfragen erst Aggregat Tabelle angepasst werden müsste
* Ziel:
    * Vermeide vordef. materialisierte Aggregate und Summen Tabellen
    * Ausfürhung von OLTP und OLAP Abfragen in einem System
    * Komplexitätsreduktion
    * Flexible Realtime Analyse
* Idee:
    * Aggregatergebnisse in den Hauptspeicher
    * Neue Aggregate die durch Änderungen enstehen zur Laufzeit errechnen und in DiffBuff ablegen
    * Für jedes SELECT FROM GROUPY BY HAVING wird eigener Eintrag erstellt
    * Aggregaterstellung verzögert bis Delta Merge
    * Aggregate nach INSERTs werden durch Laufzeit Aggregation im DiffBuff aufgefangen
    * Aggregate nach UPDATEs und DELETEs durch Bitvektoren im Hauptspeicher und DiffBuff kenntlich gemacht und neu berechnet
    * Studenten werden mit ellenlangen trockenen Erklärungen verarscht
    * *__Hier Beispiel einfügen....__*

#### Aggregat Cache Auflösung
* ziel:
    * Limitieren d. Cache Wachstums
    * Halte Maintenance niedrig
* Kostenbasierte Ansätze
    * Anzahl Tupel
    * Anzahl Berechnungen
    * Ergebnismenge
    * Anzahl Invalidierungen des Hauptspeichers

    
#### Logging und Recovery
* Ausfallsicherheit
* Notwendigkeit bei Rechnerausfall auf letzten "commited" Zustand zugreifen zu können
* Varianten:
    * Logfile auf Backup Medium schreiben
    * Replication des kompletten Servers auf replizierte DB
* Ziel: Möglichst kurze Recoveryzeit ohne Datenverlust

#### Logging
* Snapshots schreibt Primärdaten periodisch auf persistenten Speicher, meist nach Merge Prozess erstellt
* Im Logging werden Update/Delete/Insert Statements mit Parametern in kompr. Form in Logfile abgelegt
* In Dict. Log werden alle Dict. Änderngen in kompr. Form abgelegt


#### Dictionary Logging
* Schreibe Dictionary Änderungsdaten entkoppelt von Transaktionen
* ValueID Vektor und Dict. Daten können separat recovered werden
* Logfile kleiner
* 3 untersch. Logs:
    * Dictionary Logs (d) für komplt. Dictionary (Tabellen ID, Spaltenindex, Value ID)
    * Value Logs (v) für Value Vektor (Transaktions ID, Tabellen ID, Zeilen ID, Value ID Vektor)
    * Transaction Logs (t) halten Transaktions ID, die nach jedem Flush erhöht wird (Junge was ist falsch mit dir)
* Vorteile bei:
    * Langen Daten die mehrfach eingetragen werden
    * Anzahl distinct values niedrig

    
#### Logging Beispiel

![transactexample](.pictures/transactexample.png)

#### Recovery
1. Lese Metadaten und Transaktions ID der letzten Transaktion aus letztem Snapshop
2. Lese Inhaltsdaten in parallelisierter Form
    1. Hauptspeicherdaten vom Snapshop
    2. Dictionary vom DiffBuff durch Einspielen d. Dictionary Logfiles
    3. Einlesen der Value ID Vektoren aus DiffBuff

#### Replikation
* IMDB vereinen OLAP und OLTP in einem System
* Rechenintensive Read-Only OLAP Transaktionen -> große Last
* Idee: lagere komplexe Read-Only Transaktionen durch Replikation in eigenes System aus
1. Nur committede Transaktionen werden repliziert
2. Reihenfolge der Transaktionen immer beibehalten
3. Bei mehreren Replicas werden die Transaktionen unterschiedlich schnell, je nach Last, nachgespielt
4. Vorteil: Master DB wird nicht beinträchtigt
5. Übliche Verzögerung <1 Sekunde

#### Transaktionswiederholung
* TAs immer atomar ausgeführt
* Sichtbar erst nach COMMIT
* TAs können gruppiert werden durch Group Commits
* Unterscheidung zwischen READ ONLY und MODIFYING Transaktionen
* Kennzeichnung in Tabelle mittels Timestamp

#### Fail-Over
* Wir wird festestellt dass Master ausgefallen? Welche Replika soll übernommen werden? Wie wird sichergestellt, dass Master letzten Stand hat?
1. Commits von Client an Replikas durchreichen
2. Bestätigung von Replikas an Master
3. Bestätigung von Master an Clients
4. Ausstehende ACKs einholen

![failover](./pictures/failover.png)

#### Fail-Over-Steuerung
1. Master sendet Heartbeat alle 50ms an Replikas
2. Gepaart mit Transaktionsdaten oder als leere Nachricht
3. Sobald alle Replikas 3 Nachrichten verpassen, -> Fail-Over initiiert
4. Replikas votieren für die Replika, die von allen anerkannt, um neuen Master zu werden
5. Diese Replika wird neuer Master
6. Alle Replikas informieren neuen Master über ihren Transaktionsstand
7. Neuer Master spielt ggfs. Fehlstand nach

#### Data Aging
* was ist der Unterschied zw. Data Aging und Data Archiving (WEN INTERESSIERT'S???)

![dataaging](./pictures/dataaging.png)

* Relevante Daten (HOT DATA) sollten nicht behandelt werden wie irrelevante Daten (COLD DATA)
* Grund:
    * Daten veralten immer schneller
    * Datenmenge steigt rapide
    * Kosten für Hauptspeicher für Speicherung irrelevanter Daten zu hoch
    * Performance bei JOINS und Aggregaten ist kot
        * Buffer, Pagelist, Indizes (Aha?)
* Idee:
    * Hot data in den hauptspeicher
    * cold data in den Sekundärspeicher (bzw Flash)
    * Applikation darf keine Abh. zu Datenalter und Speicherort haben
* Problem:
    * Vorab nicht immer klar wie alte und neue Daten unterschiedlich
* Lösung:
    * Analyse typischer SQL Abfragen und Datentreffer zu Laufzeit
    * Erstellung v. Nutzungsstatistiken
    * Festlegen von dynsmischen Aging Regeln
    * Horizontale (projektion) und vertikale (selektion) Datentrennung
    
![agingexample](./pictures/agingexample.png)

#### Zusammenfassung Data Aging -- Wenn ich was lerne dann das... danke Matthias 
* Reduktion der Daten im Haupspeicher auf Hot Data
* Identifikation dieser Daten durch Analyse der Applikation (Queries + Statistiken)
* Verlagerung v. Cold Data auf günstigeren Sekundärspeicher (Flash)
* Applikation kennt Hot/Cold Data Unterscheidung NICHT
* Vorteil: Bessere Performance, günstiger
* Nachteil: Zusätzliche Intelligenz im DBMS

#### Datenbank Reorganisation
* Häufiges Problem: DB Schema / Datenlayout ändern sich während Betrieb
* Durch: Neue DB Version, Anpassung Tabelle/Indizes wegen Performance / Schema Anpassung durch Applikationsänderung
* __Row Store:__
    * Datensätze fast immer Seite an Seite auf DB Page -> Änderung komplette Neuverteilung der Datensätze auf einzelne Seiten
    * Komplette Sperrung der Tabelle während Reaorganisation -- evtl. herunterfahren kompletter DB
    * Kleine Änderungen evtl schon extreme Auswirkungen
* Mögliche Lösung: Neue Tabelle <Table'> anlegen -> enthält nur neuen bzw. veränderten Datentyp
    * Neue+Ursprüngliche Tabelle in ein View legen
* Nachteile:
    * Ggf. schlechtere performance weil verteilte Daten
    * Overhead für Verwaltung
    * Komplexität steigt
* __Column Store:__
    * Jede einzelne Spalte völlig unabh. von Nachbarspalten -> hinzufügen/verlängern/ändern d. Datentyp problemlos möglich da eigener Speicherbereich
    * Kein Tabellenlock
    * Nur DDIC muss kurz gesperrt werden

#### Multi-Tenancy

*  __Single Tenancy:__
    * Jeder Kunde hat eigenes DBMS mit dedi. Server
    * Vorteil: volle exklusive Verfügbarkeit
    * Nachteil: Hohe Kosten, nur teilweise Nutzung
*  __Multi Tenancy:__
    * Kunden teilen sich Resourcen auf Server
    * Vorteil: bessere Auslasstung, niedrigere Kosten
    * Nachteil: ggfs. Konkurrenz um Ressourcen und technische Abhängigkeiten
*  __Teilbar:__
    * Server
        * + Jeder Kunde eigene DB
        * + Jeder Kunde eigener DB Prozess
        * + Gute Isolation
        * + Leichte Migration auf andere Maschine
        * - Kosten
        * - Keine Simultanadministration
    * Datenbank
        * + Jeder Kunde eigene Tabellen
        * + DB und Prozesse werden geteilt
        * + Bessere Speicher- und Maschinenauslastung
        * + Simultanadministration möglich
        * + Niedrige Kosten
        * - Keine Isolation
    * Tabelle
        * + DB, DB Prozesse und Tabellen geteilt
        * + Jeder Kunde eigene Datensätze gek. mit TenantID oder Mandant
        * + Maximale Ressourcenauslatung
        * + Simultanadministration möglich
        * + niedrige Kosten
        * - Keine Isolation

#### Multi-Tenancy Zusammenfassung
* Shared-Database oder Shared-Tables sind STATE-OF-THE-ART Ansätze im Cloud Computing
* Vorteile: optimale Ressourcenausnutzung und damit niedrige Kosten
* Alle Kunden gleichzeitig mit neuen Releases versorgt
* On-The-Fly Reorgs durch Kunden individuelle Änderungen ohne Downtime nur bei IMDB möglich
