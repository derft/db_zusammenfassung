- [Dateiorganisationsform vs. Zugriffspfad](#dateiorganisationsform-vs-zugriffspfad)
- [Primär vs. Sekundärschlüssel](#prim-r-vs-sekund-rschl-ssel)
- [Primärindex vs. Sekundärindex](#prim-rindex-vs-sekund-rindex)
- [Dünn- vs Dichtbesetzter Index](#d-nn--vs-dichtbesetzter-index)
- [Geclusterter vs. nicht-geclusterter Index](#geclusterter-vs-nicht-geclusterter-index)
- [Speicherformen](#speicherformen)
- [Heap-Organisation](#heap-organisation)
- [Sequentielle Speicherung](#sequentielle-speicherung)
- [Indexsequenzielle Dateiorganisation](#indexsequenzielle-dateiorganisation)
- [Indexsequenzielle Dateiorganisation: Mehrstufiger Index](#indexsequenzielle-dateiorganisation--mehrstufiger-index)
- [Probleme indexsequentieller Dateien](#probleme-indexsequentieller-dateien)
- [Indexiert-nichtsequentieller Zugriffspfad...](#indexiert-nichtsequentieller-zugriffspfad)
- [B-Bäume](#b-b-ume)
- [Klassische Hash-Verfahren](#klassische-hash-verfahren)
- [Einsatz von Hash-Verfahren in DBs](#einsatz-von-hash-verfahren-in-dbs)
- [Hashfunktion](#hashfunktion)
- [Hash-Verfahren für Datenbanken](#hash-verfahren-f-r-datenbanken)
- [Probleme bei Grundversion Hashverfahren](#probleme-bei-grundversion-hashverfahren)
- [Dynamisches Hashing](#dynamisches-hashing)
- [Überblick über Speicherstrukturen](#-berblick--ber-speicherstrukturen)
- [Mehrdimensionale Speichertechniken](#mehrdimensionale-speichertechniken)

#### Dateiorganisationsform vs. Zugriffspfad
* Dateiorganisationsform
    * Form der Speicherung der internen Relation
    * __Grundlegende__ Dateiorganisationsform (ohne Index):
        * Heap-Organisation: unsortierte Speicherung
        * Sequenzielle Organisation: sortiert Speicherung
        * Hash-Organisation: gestreute Speicherung
* Zugriffspfad
    * jede über die grundlegende Dateiorganisationsform hinausgehende Zugriffsstruktur
    * Wie jede Indexdatei über eine interne Relation

#### Primär vs. Sekundärschlüssel
* Primärschlüssel
 * Wesentliche Eigenschaft: Duplikatfreiheit dieser Attributwerte
 * Identifikation der Attributmenge
 * Verknüpfung von Relationen
* Sekundärschlüssel
 * Neben Primärschlüssel zusätliches Suchkriterium
 * Kann eins oder mehrere Attribute umfassen
 * Nicht immer unique

#### Primärindex vs. Sekundärindex
* Pro interne Relation ein __Primärindex__, mehrere Sekundärindizes
* Primärindex: ist ein Zugriffspfad auf die interne Relation
* Primärindex -> über Primärschlüssel definiert
* __Sekundärindex__: jeder weitere zugriffspfad
* Nicht jede interne Relation muss Primärindex besitzen

#### Dünn- vs Dichtbesetzter Index
*  __Dünn__besetzter Index:
 * Nur einige Datensätze im Index
 * Interne Relation sortiert nach den Zugriffsattributen -> Im Index reicht 1 Eintrag pro Seite
* __Dicht__besetzter Index:
 * Jede interne Relation erhält einen Eintrag in Indexdatei
 * Ermöglicht Bearbeitung v. einigen Anfragen ohne Zugriff auf gespeicherte Tupel (Gib mir alle Kunden-IDs größer X)
 * Index nimmt weniger Speicherplatz ein als Originaldatei

#### Geclusterter vs. nicht-geclusterter Index
*  __Geclustert__:
 * In gleicher Form sortiert wie interne Relation
  * Kundenaten nach Kundennummer sortiert -> Indexdatei über Attribut KNr clustern
 * Nicht unbedingt dünn besetzt
* __Nicht-geclusterter Index:__
 * anders organisiert als interne Relation
 * Bspw Sekundärindex für Kundendaten-Relation über Attribut Name, Datei selbst aber nach KNr sortiert -> Sekundärindex ist nicht geclustert

#### Speicherformen
![Speicherformen](./pictures/speicherformen.png)

#### Heap-Organisation
* Unsortiert auf'n Haufen schmeißen
* Extrem schnelles insert (Genug Platz -> Anhängen, sonst Seite holen und rein da)
* Einfügen verschiebt nie
* Maximaler Aufwand bei lookup und delete (weil sequentielles durchsuchen, daher meist mir Indizes eingesetzt)

#### Sequentielle Speicherung
* Sortiert (nach z.B. Kundenummer) abgelegt
* Sortierung bei __Insert__ muss eingehalten werdne, komplizierter
* Zunächst Seite suchen, dann (wahrsch) zwischen 2 ex. Datensätzen einsortieren -> nachfolgende verschieben, im header-feld offset der TID-Zeiger verändern
* einfacher machen durch füllen der Seiten nur bis zu gewissem Grad (bspw 66%)

#### Indexsequenzielle Dateiorganisation
* Sequenzielle Dateiorganisation erzgänzt durch Indexdatei
* Sortierung Datei & Indexdatei stimmen überein

![Indexsequenzielle Dateiorganisation](./pictures/indexseq.png)

* Problem beim Aufbau der Indexdatei:
 * Stark wachsende Datenmengen: Indexdatei kann aus vielen Seiten bestehen
 * verkettete Liste von Seiten (kennt nächste aber nicht vorherige)
 * Indexseiten müssen sequentiell durchsucht werden

#### Indexsequenzielle Dateiorganisation: Mehrstufiger Index
* Indexdatei ebenfalls indexsequentiell verwalten -> Zweistufiger Index
* Das auch noch mal i.seq. verwalten -> Mehrstufiger Index
* Ideal: Index höchster Stufe auf einer einzigen Seite

![krass](./pictures/multidimensionale_kraftfelder.png)

#### Probleme indexsequentieller Dateien
* Stark wachsend s.O.
* Start schrumpfende Dateien: nur zögernde Verringerung der Index- und Hauptdatei-Steien
 * Seite muss komplett leer sein
* Nach vielen Änderungsoperationen
 * Unausgeglichene Seiten d. Hauptdatei
 * zu lange Zugriffszeit

* Delete: lookup für Seite, Löschbit auf 0. Wenn erster Satz: Index anpassen, falls Seite dann leer -> zurück an Freispeicherverwaltung
* Insert: lookup für Seite. Falls Platz: Reinsortieren, falls nicht: Neue Seite anfordern, Sätze der zu vollen seite __gleichmäßig__ auf alte und neue Seite verteilen, neuer Indexeintrag

#### Indexiert-nichtsequentieller Zugriffspfad...

![waow](./pictures/sec_devops_waterfall_buzzword.png)

#### B-Bäume
* Indexbaum ist B-Baum d. Ordnung _m_, wenn folgendes:
    * Perfekt ausgeglichen: Alle Blattseiten liegen auf gleicher Stufe
    * Anzahl Elemente: 
        * Jede Seite außer Wurzel min. _m_ Elemente
        * Jede Seite höchsten _2m_ Elemente
    * Anzahl Schlüssel: Jede Seite (Außer Blattseite) mit _i_ Elementen hat _i+1_ Nachfolger
*  __*ORDNUNG*__: minimale Anzahl der Einträge af den Indexseiten, außer Wurzelseite
 * 2*Ordnung = maximale Anzahl der Einträge
* einfache, schnelle Algo. zum Suchen/einfügen/löschen
* Garantiert 50% Speicerplatzausnutzung
* _Anderer Krempel der Summary ist (angeblich) nicht Klausurrelevant..._

#### Klassische Hash-Verfahren
* Daten in Hashtabelle gespeichert
* Praxis: tabelle als array
* Hashfunktion: Mathematische Funktion, berechnet zu jedem Datensatz einen Hashwert
 * Schlüssel -> berechnen d. Hashwerts
 * Hashwert = Adresse (Bucket) in Tabelle verwendet
* Ideal: Jedes Objekt eigener Bucket
* hashing muss aber nicht eindeutig sein, gleiche Werte = Kollision -> spez. Behandlung

#### Einsatz von Hash-Verfahren in DBs
* Einsatz als Datenbankindex:
 * Schlüsselwert auf Bucket-Adresse abgebildet
 * Statt Post in Array: Hintergrundseiten als Speicherplatz
 * Bucket = Speicherbereich von ein/mehreren Seiten
 * Überlauf v. Seiten möglich durch viele Kollisionen
 * Speicherstelle v. Datensatz mit einzigem Zugriff lesbar

#### Hashfunktion
* Verbreitete Hashfunktion: _h(k) = k mod m_ (Verbreitet bei wem...?)

#### Hash-Verfahren für Datenbanken
![Tamara...](./pictures/tamara.png)
>>>
*  __Operationen:__
 * __Suchen:__ h(w) berechnen
  * zeiger aus hashverzeichnis holen
  * erste Seite des Buckets über Zeiger holen
  * Sätze auf Seite durchsuchen
  * _w_ nicht gefunden -> nächste Seite
  * Alles durchsucht: Satz nicht vorhanden
 * __Ändern:__ Suchen von _w_ mittels lookup
  * Attribut Teil des Primärschlüssels? Satz löschen, neu einfügen
  * Sonst: Wert einfach ändern
 * __Einfügen:__ Suchen von _w_ mittels lookup
  * Satz gefunden: Fehler (Bei Primärindex)
  * Satz nicht gefunden
   * In Seite mit Bild von _w_ wird letzter Satz gesucht
   * Einzufügender Satz dahinter eintragen
 * __Löschen:__ suchen von _w_ mittels lookuip
  * Satz gefunden -> Löschen (Bit auf 0)
>>>

#### Probleme bei Grundversion Hashverfahren
* Leistungsfähigkeit stark abhängig von Hashfunktion
 * Schlechte Wahl -> fast alle Datensätze auf gleicher Seite
* Anpassung der Hashfunktion an wachsende und schrumpfende Datenmengen: Fehldene Dynamik
 * Zu großer Speicherbereich erlaubt Wachstum aber schlechte Speicherauslastung
 * Gut ausgelasteter Speicherbereich: schlechte Wachstumsperformance
* Vergrößterung des Speicherbereichs und damit Änderung der Hashfunktion -> Alle Datensätze neu hashen
* Alternative: dynsmische Hashverfahren

#### Dynamisches Hashing
* Automatische Vergrößterung/Verkleinerung d. Bildbereichs
* Hashfunktion hat dennoch feste Berechnungsvorschrift
* Kein komplett neues hashen erfordlich
* Lineares Hashing, Erweiterbares Hashing, Spiralhashing, Kombinierte Methoden (Tolle Aufzählung Matthias)

#### Überblick über Speicherstrukturen

![danke](./pictures/overview_speicherscheisse.png)

>>>
#### Mehrdimensionale Speichertechniken
* Bisherige Verfahren sind eindimensional
* Mehrdimensionale Speichertechniken:
 * Erlauben symm. Zugriff über mehrere Atrribute
 * Anzahl d. unterstützten Attribute -> Anzahl der Dimensionen des Datenraums
* Geometrische Zugriffsstrukturen:
 * Für Geoinformationssysteme, Tourenplanung, Positionsunterstützung...
 * R-Baum, R+-Baum, BSP-Baum... (aha)
* hochdimensionale Daten:
 * Für Multimedia-Daten wie Bild,Audio,Video
 * R-Bäume mit Anpassung auf hohe Dimensionen, X-Bäume (spooky)
>>>

![super](./pictures/super.jpg)
