## Kapitel 1 - Einführung

- [Kapitel 1 - Einführung](#kapitel-1---einf-hrung)
    + [Abstrantionsebenen zur Entkopplung User- / gespeicherte Daten:](#abstrantionsebenen-zur-entkopplung-user----gespeicherte-daten-)
    + [Was ist relationales Datenmodell](#was-ist-relationales-datenmodell)
    + [Welche Normalformen gibt es?](#welche-normalformen-gibt-es-)
    + [Garantieren referentieller Integrität](#garantieren-referentieller-integrit-t)
    + [Was bedeutet "Tabelle hat Grad 45 und Relation 5"](#was-bedeutet--tabelle-hat-grad-45-und-relation-5-)

*** 
    
* Vergleich DBMS - Filesystems

![Tabelle](./pictures/dbms_filesys.png)

#### Abstrantionsebenen zur Entkopplung User- / gespeicherte Daten:

![Abstrantionsebenen](./pictures/teilung.png)

* Sichten stellen Teilmenge der Information bereit
* Datenbankschema in logischer Ebene (Welche Daten sind gespeichert) (konzeptionelle Ebene)
* Wie/wo sind Daten gespeichert (physische Ebene)

#### Was ist relationales Datenmodell
* mathematische Beschreibung einer Tabelle
* Operationen auf Relationen durch relationale Algebra bestimmt

![Realtionales Datenmodell](./pictures/relational.png)

#### Welche Normalformen gibt es?
* 1 NF -- Jedes Attribut d. Relation muss atomaren Wertebereich haben, frei von Wiederholungsgruppen
* 2 NF -- 1. NF und kein Nichtschlüsselattribut funktional abhängig von einer echten Teilmenge eines Schlüsselkandidaten --> bzw jedes nicht-primäre Attribut (nicht Teil eines Schlüssels) ist jeweils von allen **ganzen** Schlüsseln abhängig
* 3 NF -- Wenn 1/2 NF, ein Nichtschlüsselattribut darf nur direkt von einem Primärschlüssel (bzw. einem Schlüsselkandidaten) abhängig sein
* BCND -- Wenn 1/2/3 NF, kein Teil eines (zusammengesetzten) Schlüsselkandidaten ist funktional abhängig von einem Teil eines anderen Schlüsselkandidaten
* 4. NF -- Wenn 1/2/3/BCNF -- gibt nur noch mehrwertige triviale Abhängigkeiten
* 5. NF -- Wenn vierte alle anderen NF, Enthält keine mehrwertigen Abhängigkeiten, die voneinander abh. sind

#### Garantieren referentieller Integrität
* NO ACTION -- löschen/ändern wird abgelehnt
* CASCADE -- lösche auch alle Tupel, die das gelöschte Tupel referenzieren
* SET NULL / SET DEFAULT -- setzt den Wert der Fremdschlüssel von darauf verw. Tupel

#### Was bedeutet "Tabelle hat Grad 45 und Relation 5"
* Die Tabelle hat 45 Attribute und 5 Tupel...

