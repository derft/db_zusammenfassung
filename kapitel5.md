- [Programmierschnitstellen](#programmierschnitstellen)
- [Stored Prcedures](#stored-prcedures)
- [Programmierschnitstellen](#programmierschnitstellen-1)
- [ODBC Überblick](#odbc--berblick)
- [JDBC Überblick](#jdbc--berblick)
- [__LI*E*TERATUR__](#--li-e-teratur--)

---

#### Programmierschnitstellen
We hackers now.

#### Stored Prcedures
* Sammlung von wiederkehrenden SQL Anweisungen zusammengefasst zu Prozedur
* Nutzt Übergabeparameter
* Überlicherweise in Datenbank (Data Dictionary) gespeichert
* Ausführung auf Server... **_WOW_**
* _extrem schnell_
* Sicher (Grants nicht nötig)
* Aufruf mit CALL <procedure> (oder EXECUTE)

#### Programmierschnitstellen
* Programmier Interfaces
    * Embedded SQL
        * SQL Statements hardcoded
        * Meist mit #SQL oder EXEC SQL gekennzeichnet
        * Mit Precompiler in die Programmiersprache übersetzt
        * danach mit Compiler und Loader in Executable _verwandelt_ (magisch)
        * Bsp: _Embedded SQL for Java_ (Aka Superaids)
    * Dynamic SQL
        * Call-Level-Interface
            * Proprietäre CLIs
            * ODBS (C, C++, Fortran, BASIC)
            * JDBC (Java)
        * Viele DBMS-Vendoren beiten eigene CLIs an
        * __J__ava/__O__pen __D__ata__b__ase __C__onnectivity
        * SQL Anweisungen per Agument an CLI-procedure übergeben
        * SQL Statements dynamisch zur Laufzeit erstellt durch Übergabe v. SQL-String an Prozedur
        * Fast alle Hersteller unterstützen O/JDBC
  
![APIs](./pictures/apis.png)

#### ODBC Überblick
* Ursprünlich v. Microsoft für Windows
* Stellt C/C++ Bib zur Verfügung (Auch Fortran oder Visual Basic)
* Neben Windows auch auf __GNU__/Linux
* Anwendungsprogramme die ODBC nutzen portabel (auch binary)
* Mehrere Datenbanken untersch. Hersteller gleichzeitig nutzbar

#### JDBC Überblick
* Javasoft/Sun entwickelt
* 2 Pakete
 * java.sql - grundlegende Klassen und Schnitstellen
 * javax.sql - erweiterte Funktionalitäten für die Arbeit mit Datenbanken
* Wichtigten Klassen sind: (Spannende und wichtige Aufzählen Matthias...)
 * java.sql.DriverManager - Einstiegspunkt, registriert Treiber und verbindet zu DB
 * java.sql.Connection - repr. Datenbankverbindung
 * java.sql.Statement - Ermöglicht Ausführung von SQL-Statements...
 * java.sql.ResultSet - verwartet Ergebnisse einer Anfrage als Relation

# __LI*E*TERATUR__

![_er_](./pictures/putin.jpg)
